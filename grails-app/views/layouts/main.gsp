<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><g:layoutTitle default="Simple blog"/></title>
    
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.min.css')}" type="text/css">
    <g:layoutHead/>
    <r:layoutResources />
  </head>
  <body>
    <div class="container">
      <div class="hero-unit">
        <h1><g:link controller="post" action="index">Simblog</g:link></h1>
        <p>This is a simple blog app written in Grails!</p>
      </div>
      
      <div class="container-fluid">
        <div class="row-fluid">
          
          <g:layoutBody/>
          
        </div>
      </div>
      
    </div>
    
    <footer class="footer">
      <div class="container">
        <p>Simple blog app written in <a href="http://www.grails.org/">Grails</a> by <a href="mailto:damian.duda.89@gmail.com">Damian Duda</a></p>
        <p>2013 at <a href="http://www.polsl.pl/Strony/Witamy.aspx">polsl</a></p>
      </div>
    </footer>
    
    <g:javascript src="bootstrap.js"/>
    <r:layoutResources />
  </body>
</html>
