<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <title>Simple blog | entry name here</title>
    <meta name="layout" content="main">
  </head>
  <body>
    <div class="span3">
      <ul class="nav nav-list">
        <li class="nav-header">Recent entries</li>
        <g:each var="post" in="${posts.sort {it.id}}">
          <li><g:link controller="post" action="show" id="${post.id}">${post.title}</g:link></li>
        </g:each>
      </ul>
    </div>

    <div class="span9">
      <h2>${post.title}</h2>

      ${post.body}

      <div style="margin-top: 20px; opacity: .7;">
        <i class="icon-comment"></i> ${post.comments.size()}
        <i style="margin-left: 20px;" class="icon-pencil"></i> ${post.author}
        <i style="margin-left: 20px;" class="icon-time"></i> ${post.created}
      </div>
      <hr class="bs-docs-separator">
      <div id="comments">
        <g:each var="comment" in="${comments.sort {it.id}}">
          <i class="icon-comment"></i> ${comment.author}
          <div class="well">
            ${comment.comment}
          </div>
        </g:each>
        
        
        <h3>Leave few words</h3>
        <hr class="bs-docs-separator">
        
        <g:form name="newComment" action="save" controller="comment" class="form-horizontal">
          <g:hiddenField name="post.id" value="${post.id}" />
          
          <div class="control-group">
            <label class="control-label" for="inputEmail">Name</label>
            <div class="controls">
              <g:textField name="author" />
            </div>
          </div>
          
          <div class="control-group">
            <label class="control-label" for="inputEmail">Email</label>
            <div class="controls">
              <g:textField name="email" />
            </div>
          </div>
          
          <div class="control-group">
            <label class="control-label" for="inputEmail">Comment</label>
            <div class="controls">
              <g:textArea name="comment" />
            </div>
          </div>
          
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn">Sign in</button>
            </div>
          </div>
        </g:form>
        
      </div>
    </div>
          
  </body>
</html>
