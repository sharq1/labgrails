<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <title>Simple blog | latest entries</title>
    <meta name="layout" content="main">
  </head>
  <body>
    <div class="span12">
      <g:each var="post" in="${posts.sort {it.id}}">
        <h2><g:link controller="post" action="show" id="${post.id}">${post.title}</g:link></h2>
        <g:if test="${post.body.size() > 255}">
          ${post.body[0..255]}...
          <g:link controller="post" action="show" id="${post.id}">read more</g:link>
        </g:if>
        <g:else>
          ${post.body}
        </g:else>
        
        <div style="margin-top: 20px; opacity: .7;">
          <i class="icon-comment"></i> ${post.comments.size()}
          <i style="margin-left: 20px;" class="icon-pencil"></i> ${post.author}
          <i style="margin-left: 20px;" class="icon-time"></i> ${post.created}
        </div>
        <hr class="bs-docs-separator">
      </g:each>
    </div>
          
  </body>
</html>
