package labgrails

class Post {
    
    static hasMany = [comments: Comment]
    
    String title
    String body
    String author
    Date created
    
    static mapping = {
        body type: "text"
    }
    
    static constraints = {
        title blank: false
        body blank: false
//        body blank: false, size: 0..65535
        author blank: false
        created blank: true
    }
}
