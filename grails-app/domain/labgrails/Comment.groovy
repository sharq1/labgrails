package labgrails

class Comment {
    
    String author
    String email
    String comment
    
    static belongsTo = [post: Post]

    static constraints = {
        author blank: false
        email email: true, blank: false
        comment blank: false
    }
}
