package labgrails

class PostController {

    def index() {
        [posts: Post.list()]
    }
    
    def show() {
        def post = Post.get(params.id)
        if (!post){
            response.sendError 404
            return
        }
        [post: post, posts: Post.list(), comments: post.comments]
    }
}
