package labgrails

class CommentController {

    def scaffold = Comment
    
    def show() {
        def comment = Comment.get(params.id)
        def postid = comment.post.id
        redirect(controller: "post", action: "show", id: postid)
    }
    
    def edit() {
    }
    def update() {
    }
    def delete() {
    }
}
